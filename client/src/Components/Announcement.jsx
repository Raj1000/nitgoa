import { useContext, useState, useRef, useEffect } from "react";
import Line from "../assets/Line.svg";
import News from "../assets/News.png";
import DynamicLinks from "../assets/Dynamic Links.png";
import Noticeboard from "../assets/Noticeboard.png";
import SynapseImg from "../assets/synapse_newsletter.jpeg";
import Tweetlogo from "../assets/Tweet.svg";
import { AppContext } from "./AppContext";
import { useTranslation } from "react-i18next";
import NitGoalogo from "../assets/NIT_Goa_logo.png";
import { BarController } from "chart.js";
import Placement from "./Placement";
// import { TwitterTweetEmbed } from "react-twitter-embed";
// import { TwitterTweetEmbed } from 'react-twitter-embed';
// import HomePageCaorusel from './HomePageCarousel/HomePageCarousel'
import MotoCarousel from "./MotoCarousel/MotoCarousel";
import moto1 from "../assets/moto1.svg";
import moto2 from "../assets/moto2.svg";
import moto3 from "../assets/moto3.svg";
import { Tweet } from 'react-tweet'
import Marquee from 'react-fast-marquee';
import "./Announcement.css";
import { useSelector, useDispatch } from 'react-redux';
import SlideShow from "./SlideShow";

import "./style.css";
import { fetchAnnouncements } from "../features/Announcement/announcement";
import { fetchNews } from "../features/News/newsSlice";
import { fetchNotices } from "../features/NoticeBoard/NoticeBoardSlice";
import { fetchTender } from "../features/Tenders/tendersSlide";
import Statistics from "./Statistics";

const Announcement = () => {
  const { t } = useTranslation();
  const { theme } = useContext(AppContext);
  const [moto, setmoto] = useState(0);
  const [data, setdata] = useState(0);
  const dispatch = useDispatch();
  const announcement = useSelector((state) => state.announcements.announcements);
  const announcementsLoading = useSelector((state) => state.announcements.loading);
  const news = useSelector((state) => state.news.news);
  const {tender} = useSelector((state) => state.tender);
  const notices = useSelector((state) => state.notices.notices);
  const newsLoading = useSelector((state) => state.news.loading);
  const handlemoto = () => {
    console.log(moto);

    if (data === 0) {
      setmoto(340);
      setdata(40);
    } else if (data === 40) {
      setmoto(690);
      setdata(80);
    } else if (data === 80) {
      setmoto(0);
      setdata(0);
    }
  };

  // const slides = [
  //   { type: "image", image: slideshow1, description: "Description 1", accentColorLink: bg_1 },
  //   { type: "video", image: video, description: "Description 5", accentColorLink: bg_4 },
  //   { type: "image", image: slideshow2, description: "Description 2", accentColorLink: bg_2 },
  //   { type: "image", image: slideshow3, description: "Description 3", accentColorLink: bg_3 },
  //   { type: "image", image: slideshow4, description: "Description 4", accentColorLink: bg_4 },
  // ];


  useEffect(() => {
    const fetchData = async () => {
      await dispatch(fetchAnnouncements());
      await dispatch(fetchNews());
      await dispatch(fetchNotices());
      await dispatch(fetchTender());
    };
    fetchData();
  }, [dispatch]);




  return (
    <div>
      <SlideShow></SlideShow>


      {/* <HomePageCaorusel className='h-40'/>     */}

      <div className={`bg-${theme}bg  pb-5 text-${theme}txt lg:px-10`}>
        <div className=" flex flex-col text-center justify-center">
          {/* <h1 className="font-dosis font-semibold	text-lg	">
            {t("announcement")}
          </h1>
          <div
            className={`w-20 mx-auto rounded-sm mt-1 bg-${theme}th px-0.5 py-0.5`}
          ></div> */}
        </div>

        <div className="p-[26px] lg:px-10">

          <div className="border-[#9F9F9F] border-[0.2px] my-2 mx-4"></div>

          <div className="flex items-center px-4 gap-3">
            <p className="hidden lg:block text-lg font-bold z-10 p-1 rounded-md">{t("announcement")}</p>
            <div className="hidden lg:block" >|</div>
            <Marquee pauseOnHover className=" lg:text-md p-1 flex-grow">
              {/* {t("announcement_content")} */}
              {/* {announcementsLoading &&
                (<p>Loading...</p>)
              } */}
              <a href={announcement?.content}>{announcement[0]?.title}</a>
            </Marquee>
          </div>

          {/* <div className={`m-4 bg-${theme}txtbg border rounded-xl p-5`}>
            <h1 className="font-dosis font-extrabold text-base mb-3">
              🚨 {t("latest")}

            </h1>
          </div> */}
          <div className="border-[#9F9F9F] border-[0.2px] my-2 mb-4 mx-4 "></div>




          {/* News and Events */}
          <div className="grid grid-cols-1  md:grid-cols-3 gap-5 ">

            <div className={`bg-${theme}txtbg text-${theme}txt w-full shadow-xl rounded-lg p-[14px] h-[400px] relative`}>

              <div className="w-full">
                <div className="flex p-2 w-full justify-center align-center">
                  <img className="w-6" src={News} alt="News" />
                  <h1 className=" font-semibold px-1">{t("news_events")}</h1>
                </div>

                <div className="list-none px-4 py-2 h-80 overflow-auto text-sm ">
                  {Array.isArray(news) && news?.map((newss, index) => (
                    <div key={index} className="py-2 border-b">
                      <a href={newss?.link} target="_blank" className=" hover:text-blue-800 hover:underline">
                        {newss?.content}
                      </a>
                    </div>
                  ))}

                </div>
                {/* <div className="absolute text-sm right-7 font-noto flex flex-row gap-1 items-center justify-items-center">
                  Read More{" "}
                </div> */}
              </div>
            </div>


            {/* <div className="border-[#9F9F9F] border-[0.2px] my-5 mx-4 md:hidden"></div> */}

            <div
              className={`bg-${theme}txtbg text-${theme}txt w-full shadow-xl rounded-lg p-[10px] h-[400px]`}
            >
              <div className="w-full h-[242px] relative">
                <div className="flex p-2 w-full justify-center align-center">
                  <img className="w-6" src={DynamicLinks} alt="Quick Links" />
                  <h1 className="font-dosis px-1 font-semibold">Tender</h1>
                </div>
                <div className="list-none px-4 py-2 h-80 overflow-auto text-sm font-dosis">
                  {Array.isArray(tender) && tender?.map((newss, index) => (
                    <div key={index} className="py-2 border-b">
                      <a href={newss?.link} target="_blank" className=" hover:text-blue-800 hover:underline">
                        {newss?.content}
                      </a>
                    </div>
                  ))}
                </div>
                {/* <div className="absolute text-sm right-7 font-noto flex flex-row gap-1 items-center justify-items-center">
                  Read More{" "}
                </div> */}
              </div>
            </div>


            {/* <div className="border-[#9F9F9F] border-[0.2px] my-5 mx-4 md:hidden"></div> */}
            <div
              className={`bg-${theme}txtbg text-${theme}txt w-full shadow-xl rounded-lg p-[10px] h-[400px] relative `}
            >
              <div className="w-full">
                <div className="flex p-2 w-full justify-center align-center">
                  <img
                    className="w-6 !text-redth"
                    src={Noticeboard}
                    alt="Notice Board"
                  />
                  <h1 className="font-dosis px-1 font-semibold">{t("notice")}</h1>
                </div>
                <div className="list-none px-4 py-2 h-80 overflow-y-auto text-sm font-dosis">
                  {Array.isArray(notices) && notices?.map((newss, index) => (
                    <div key={index} className="py-2 border-b">
                      <a href={newss?.link} target="_blank" className=" hover:text-blue-800 hover:underline">
                        {newss?.content}
                      </a>
                    </div>
                  ))}
                </div>
                {/* <div className="absolute right-7 flex flex-row gap-1 items-center justify-items-center font-noto text-sm">
                  Read More{" "}
                </div> */}
              </div>
            </div>
          </div>
        </div>
        <div className="border-[#9F9F9F] border-[0.2px] my-5 mx-4 md:hidden"></div>

        <div className="grid grid-cols-1 md:grid-cols-3 gap-5 lg:px-10 px-1">


          <Placement />

          <div className="border-[#9F9F9F] border-[0.2px] my-5 mx-4 md:hidden"></div>

          <div className={`bg-${theme}txtbg text-${theme}txt w-full shadow-xl rounded-lg p-[10px]`}>
            <div className="flex items-center justify-center text-xl">
              <i className={`fa-newspaper ${theme === "dark" ? "fa-regular" : "fa-solid"}  mr-[10px]`}></i>
              <h1 className="font-dosis font-semibold  ">
                {t("synapse")}
              </h1>
            </div>
            <div className="h-full w-full flex flex-col items-center">
              <a href="https://www.nitgoa.ac.in/static/Biannual_Newsletter_31oct2023.pdf" target="_blank">
                <img className="object-cover" src={SynapseImg} />
              </a>
            </div>
          </div>
          <div className="border-[#9F9F9F] border-[0.2px] my-5 mx-4 md:hidden"></div>

          <div className={`bg-${theme}txtbg text-${theme}txt w-full shadow-xl rounded-lg p-[10px] pb-8`}>
            <div className="flex items-center justify-center p-2">
              <img
                className={`w-5 ${theme === "dark" ? "invert" : ""
                  } mr-1`}
                src={Tweetlogo}
                alt="Tweetlogo"
              />
              <h1 className="font-dosis font-semibold text-xl ">
                {t("tweet")}
              </h1>
            </div>
            <div className="overflow-auto">
              <div className="flex flex-col items-center justify-center">
                <div className="flex flex-col items-center justify-center">
                  {/* <img src={NitGoalogo} alt="NIT-Goa-logo" className="max-w-[100px] p-2" /> */}
                  {/* <a
                    className="twitter-timeline"
                    data-width="300"
                    data-height="400"
                    href="https://twitter.com/NITGoa_Official?ref_src=twsrc%5Etfw"
                  >
                    Tweets by NITGoa_Official
                  </a> */}
                  {/* <a
                    className="twitter-timeline"
                    data-width="300"
                    data-height="400"
                    href="https://twitter.com/NITGoa_Official"
                  >
                    Follow @NITGoa_Official on Twitter <i className="fa-solid fa-caret-right animation"></i>
                  </a> */}

                  {/* <a className="twitter-timeline" href="https://twitter.com/NITGoa_Official?ref_src=twsrc%5Etfw">Tweets by NITGoa_Official</a> */}
                  <a className="twitter-timeline"
                    href="https://twitter.com/NITGoa_Official"
                    data-width="300"
                    data-height="400">
                    Tweets by @NITGoa_Official
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <MotoCarousel />

        <div className={`hidden lg:flex mx-[24px] h-[400px] bg-${theme}txtbg rounded-[30px] shadow-lg overflow-hidden pl-[50px] mt-4`}>
          <div className="overflow-hidden w-[400px] shrink-0 bg-white">
            <div className="overflow-hidden w-[400px] h-full py-[52px] pr-[50px] bg-white">
              <div
                className={`flex -translate-x-[${moto}px] duration-150 ${data === 0 ? "translate-x-[20px]" : ""
                  }`}
              >
                <img className="w-[400px] h-full" src={moto1} alt="About us" />
                <img className="w-[400px] h-full" src={moto2} alt="Vision" />
                <img className="w-[400px] h-full" src={moto3} alt="Mission" />
              </div>
            </div>
          </div>
          <div>
            <div className={`h-full w-[1px] bg-${theme}txt ml-[139px] relative `}>
              <div
                onClick={() => {
                  handlemoto();
                }}
                className={`absolute h-[100px] w-[100px] rounded-full bg-${theme}txt top-1/2 -translate-x-1/2 -translate-y-1/2 flex justify-center items-center cursor-pointer `}
              >
                <p className={`text-2xl text-${theme}txtbg z-10`}>NEXT</p>
                <div className={`absolute bg-black h-[100px] w-[100px] rounded-full hover:animate-ping bg-${theme}txt`}></div>
              </div>
            </div>
          </div>
          <div className="font-dosis flex flex-col items-center justify-center">
            <div className="overflow-hidden h-[40px] ">
              <div
                className={`mb-[50px] -translate-y-[${data}px] duration-150 justify-center items-center flex flex-col`}
              >
                <h1 className="text-4xl	font-bold	">ABOUT</h1>
                <h1 className="text-4xl	font-bold	">VISION</h1>
                <h1 className="text-4xl	font-bold	">MISSION</h1>
              </div>
            </div>
            <div className="text-xl font-semibold	ml-[121px] mr-[84px] text-center transform duration-150">
              {data == 0
                ? "The National Institute of Technology Goa (NIT Goa) is a premier national-level technical institute in India established in 2010 by an act of parliament (NIT Act, 2007 and NIT (Amendment) Act, 2012). NIT Goa is an autonomous institute functioning under the aegis of Ministry of Education (MoE), Government of India, and has been declared an \" Institute of National Importance \"."
                : data == 40
                  ? "National Institute of Technology Goa shall emerge as one of the nation's pre-eminent institutions. Through its excellence, it shall serve the Goan society, India and humanity at large with all the challenges and opportunities."
                  : "NIT Goa strives for quality faculty, good students and excellent infrastructure Strives for excellence, through dissemination, generation and application of knowledge by laying stress on interdisciplinary approach in all the branches of Science, Engineering, Technology, Humanities and Management with emphasis on human values and ethics."}
            </div>
          </div>
        </div>










        <Statistics />
      </div>
      <div className="hidden -translate-y-[0px] -translate-y-[40px] -translate-y-[80px] translate-x-[10px]  -translate-x-[340px] -translate-x-[341px] -translate-x-[690px]"></div>
    </div>
  );
};

export default Announcement;
